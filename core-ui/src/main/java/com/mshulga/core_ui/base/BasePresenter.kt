package com.mshulga.core_ui.base

import com.mshulga.core_navigation.FlowRouter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import moxy.MvpPresenter
import moxy.MvpView

/**
 * @author mshulga
 * @since  2019-08-09
 */
open class BasePresenter<View : MvpView>(protected val router: FlowRouter) : MvpPresenter<View>(),
    CoroutineScope by MainScope() {

    override fun onDestroy() {
        cancel()
    }

    fun onBackPressed() = router.exit()
}