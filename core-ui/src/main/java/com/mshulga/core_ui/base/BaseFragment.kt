package com.mshulga.core_ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mshulga.core_utils.di.Scopes
import moxy.MvpAppCompatFragment
import toothpick.Scope
import toothpick.ktp.KTP

/**
 * @author mshulga
 * @since  2019-08-26
 */
abstract class BaseFragment : MvpAppCompatFragment() {

    abstract val layoutRes: Int

    protected lateinit var scope: Scope
        private set

    private lateinit var scopeName: String
    private val parentScopeName: String by lazy {
        (parentFragment as? BaseFragment)?.scopeName ?: Scopes.APPLICATION_SCOPE
    }

    private var isInstanceStateSaved: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        openScope(savedInstanceState)
        super.onCreate(savedInstanceState)
    }

    private fun openScope(savedInstanceState: Bundle?) {
        scopeName = savedInstanceState?.getString(SCOPE_NAME_SAVE_KEY) ?: getBasicScopeName()
        if (KTP.isScopeOpen(scopeName)) {
            scope = KTP.openScopes(parentScopeName, scopeName)
        } else {
            scope = KTP.openScopes(parentScopeName, scopeName)
            installModules(scope)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(layoutRes, container, false)

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        isInstanceStateSaved = true
        outState.putString(SCOPE_NAME_SAVE_KEY, scopeName)
    }

    override fun onStart() {
        super.onStart()
        isInstanceStateSaved = false
    }

    override fun onResume() {
        super.onResume()
        isInstanceStateSaved = false
    }

    override fun onDestroy() {
        super.onDestroy()
        handleLifecycle()
    }

    private fun handleLifecycle() {
        if (isInstanceStateSaved) {
            isInstanceStateSaved = false
            return
        }
        if (activity?.isFinishing == true || isRealyRemoving()) KTP.closeScope(scope.name)
    }

    private fun isRealyRemoving(): Boolean {
        var isAnyParentRemoving = false
        var parent = parentFragment
        while (!isAnyParentRemoving && parent != null) {
            isAnyParentRemoving = parent.isRemoving
            parent = parent.parentFragment
        }
        return isRemoving || isAnyParentRemoving
    }

    protected open fun installModules(scope: Scope) {}

    abstract fun onBackPressed()

    private fun getBasicScopeName() = "${javaClass.simpleName}$$${hashCode()}"

    companion object {
        private const val SCOPE_NAME_SAVE_KEY = "SCOPE_NAME_SAVE_KEY"
    }
}