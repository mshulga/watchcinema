package com.mshulga.core_ui.di

import com.mshulga.core_navigation.FlowRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

/**
 * @author mshulga
 * @since  2019-08-27
 */
class FlowModule(appRouter: Router) : Module() {

    init {
        val cicerone = Cicerone.create(FlowRouter(appRouter))
        bind(FlowRouter::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)
    }
}