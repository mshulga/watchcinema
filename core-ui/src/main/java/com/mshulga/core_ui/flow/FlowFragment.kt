package com.mshulga.core_ui.flow

import android.os.Bundle
import com.mshulga.core_ui.R
import com.mshulga.core_ui.base.BaseFragment
import com.mshulga.core_ui.di.FlowModule
import moxy.MvpView
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Scope
import toothpick.Toothpick
import javax.inject.Inject

/**
 * @author mshulga
 * @since  2019-08-27
 */
abstract class FlowFragment : BaseFragment(), MvpView {

    override val layoutRes: Int = R.layout.fmt_main

    @Inject
    lateinit var navigatorHolder: NavigatorHolder
    @Inject
    lateinit var router: Router

    private val currentFragment
        get() = childFragmentManager.findFragmentById(R.id.fragmentsContainer) as? BaseFragment

    private val navigator: Navigator by lazy {
        object : SupportAppNavigator(activity!!, childFragmentManager, R.id.fragmentsContainer) {
            override fun activityBack() {
                router.exit()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toothpick.inject(this, scope)
    }

    override fun installModules(scope: Scope) {
        scope.installModules(FlowModule(scope.getInstance(Router::class.java)))
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed()
    }

}