package com.mshulga.core_ui.flow

import com.mshulga.core_navigation.FlowRouter
import com.mshulga.core_ui.base.BasePresenter
import moxy.MvpView
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * @author mshulga
 * @since  2019-08-27
 */
abstract class FlowPresenter<View : MvpView>(router: FlowRouter) : BasePresenter<View>(router) {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        router.newRootScreen(getLaunchScreen())
    }

    abstract fun getLaunchScreen(): SupportAppScreen
}