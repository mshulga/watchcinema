package com.mshulga.core_prefs

import android.content.Context
import javax.inject.Inject

/**
 * @author mshulga
 * @since  2019-10-15
 */
class SharedPrefs @Inject constructor(private val context: Context) {
    private val prefs by lazy {
        context.getSharedPreferences(
            APP_PREFERENCES,
            Context.MODE_PRIVATE
        )
    }

    var sessionId: String?
        get() = prefs.getString(SESSION_ID_KEY, null)
        set(value) = prefs.edit().putString(SESSION_ID_KEY, value).apply()

    companion object {
        private const val APP_PREFERENCES = "APP_PREFERENCES"
        private const val SESSION_ID_KEY = "SESSION_ID_KEY"
    }
}