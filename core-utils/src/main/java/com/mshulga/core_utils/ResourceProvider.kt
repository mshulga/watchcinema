package com.mshulga.core_utils

import android.content.Context
import androidx.annotation.StringRes
import javax.inject.Inject

/**
 * @author mshulga
 * @since  2019-08-23
 */
class ResourceProvider @Inject constructor(private val context: Context) {
    fun getString(@StringRes id: Int) = context.getString(id)
}