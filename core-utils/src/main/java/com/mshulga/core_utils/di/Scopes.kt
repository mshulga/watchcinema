package com.mshulga.core_utils.di

/**
 * @author mshulga
 * @since  2019-08-22
 */
object Scopes {
    const val APPLICATION_ROOT_SCOPE = "APPLICATION_ROOT_SCOPE"
    const val APPLICATION_SCOPE = "APPLICATION_SCOPE"
}