package com.mshulga.core_utils.extentions

import android.view.View

/**
 * @author mshulga
 * @since  2019-10-15
 */
fun View.show() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}