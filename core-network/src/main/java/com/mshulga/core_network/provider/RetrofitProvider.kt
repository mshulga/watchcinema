package com.mshulga.core_network.provider

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.mshulga.core_network.data.BaseUrl
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Provider

/**
 * @author mshulga
 * @since  2019-10-15
 */
class RetrofitProvider @Inject constructor(
    @BaseUrl private val baseUrl: String,
    private val client: OkHttpClient
) : Provider<Retrofit> {

    override fun get(): Retrofit {
        val contentType = "application/json".toMediaType()
        val json = Json(
            JsonConfiguration.Stable.copy(
            ignoreUnknownKeys = true
        ))
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(json.asConverterFactory(contentType))
            .client(client)
            .build()
    }

}