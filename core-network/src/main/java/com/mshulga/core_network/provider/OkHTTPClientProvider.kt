package com.mshulga.core_network.provider

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Inject
import javax.inject.Provider

/**
 * @author mshulga
 * @since  2019-10-15
 */
class OkHTTPClientProvider @Inject constructor(): Provider<OkHttpClient> {
    override fun get(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()
    }

}