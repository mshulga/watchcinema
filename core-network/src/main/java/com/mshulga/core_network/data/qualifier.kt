package com.mshulga.core_network.data

import javax.inject.Qualifier

/**
 * @author mshulga
 * @since  2019-10-15
 */
@Qualifier
annotation class BaseUrl