package com.mshulga.watchcinema

import android.app.Application
import com.mshulga.core_utils.di.Scopes
import com.mshulga.watchcinema.di.ApplicationRootModule
import toothpick.Toothpick
import toothpick.configuration.Configuration
import toothpick.ktp.KTP

/**
 * @author mshulga
 * @since  2019-08-22
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initToothpick()
        openAppScope()
    }

    private fun openAppScope() {
        KTP.openScope(Scopes.APPLICATION_ROOT_SCOPE)
            .installModules(ApplicationRootModule(this))
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }
    }

}