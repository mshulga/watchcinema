package com.mshulga.watchcinema

import android.os.Bundle
import com.mshulga.core_prefs.SharedPrefs
import com.mshulga.core_utils.di.Scopes
import com.mshulga.watchcinema.di.*
import moxy.MvpAppCompatActivity
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.ktp.KTP
import javax.inject.Inject

class AppActivity : MvpAppCompatActivity() {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var prefs: SharedPrefs

    private val currentFragment: com.mshulga.core_ui.base.BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.mainContainer) as? com.mshulga.core_ui.base.BaseFragment

    private val navigator: Navigator = SupportAppNavigator(this, supportFragmentManager, R.id.mainContainer)

    override fun onCreate(savedInstanceState: Bundle?) {
        initAppScope()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_main)
        goToStartScreen()
    }

    private fun goToStartScreen() {
        val startScreen = if (prefs.sessionId != null) Screens.ListContainerFlow else Screens.AuthFlow
        router.newRootScreen(startScreen)
    }

    private fun initAppScope() {
        KTP.openScopes(Scopes.APPLICATION_ROOT_SCOPE, Scopes.APPLICATION_SCOPE)
            .installModules(NetModule(), NavigationModule(), StorageModule(), UtilsModule())
            .inject(this)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}
