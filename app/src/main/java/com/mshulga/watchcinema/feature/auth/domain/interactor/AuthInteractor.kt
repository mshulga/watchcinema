package com.mshulga.watchcinema.feature.auth.domain.interactor

import com.mshulga.watchcinema.feature.auth.data.repository.AuthRepository
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationException
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationSessionEntity
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationTokenEntity
import javax.inject.Inject

/**
 * @author mshulga
 * @since  2019-08-09
 */
class AuthInteractor @Inject constructor(private val authRepository: AuthRepository) {

    suspend fun getNewAuthenticationToken(): AuthenticationTokenEntity {
        val token = authRepository.getNewAuthenticationToken()
        if (token.requestToken.isEmpty()) {
            throw AuthenticationException(INVALID_REQUEST_TOKEN_ERROR_MESSAGE)
        }
        return token
    }


    suspend fun createNewSession(requestToken: String): AuthenticationSessionEntity {
        val sessionEntity = authRepository.createNewSession(requestToken)
        if (sessionEntity.sessionId.isEmpty()) {
            throw AuthenticationException(INVALID_SESSION_ID_ERROR_MESSAGE)
        }
        return sessionEntity
    }

    companion object {
        private const val INVALID_REQUEST_TOKEN_ERROR_MESSAGE =
            "Invalid authentication request token"
        private const val INVALID_SESSION_ID_ERROR_MESSAGE =
            "Invalid authentication session identifier"
    }
}