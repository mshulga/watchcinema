package com.mshulga.watchcinema.feature.auth.data.model

import com.mshulga.core_network.data.BaseResponse
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * @author mshulga
 * @since  2019-08-05
 */
@Serializable
data class CreateTokenResponse(
    @SerialName("request_token") var requestToken: String? = null,
    @SerialName("expires_at") var expireAt: String? = null,
    @SerialName("success") var success: Boolean = false
) : BaseResponse

@Serializable
data class CreateSessionResponse(
    @SerialName("session_id") var sessionId: String? = null,
    @SerialName("success") var success: Boolean = false
) : BaseResponse