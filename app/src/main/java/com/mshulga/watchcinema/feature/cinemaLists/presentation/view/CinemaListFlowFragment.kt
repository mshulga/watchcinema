package com.mshulga.watchcinema.feature.cinemaLists.presentation.view

import com.mshulga.core_ui.flow.FlowFragment

/**
 * @author mshulga
 * @since  2019-08-28
 */
class CinemaListFlowFragment : FlowFragment() {
    companion object {
        fun getInstance() = CinemaListFlowFragment()
    }
}