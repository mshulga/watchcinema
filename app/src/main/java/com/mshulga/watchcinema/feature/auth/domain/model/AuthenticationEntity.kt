package com.mshulga.watchcinema.feature.auth.domain.model

/**
 * @author mshulga
 * @since  2019-08-09
 */
data class AuthenticationTokenEntity(val requestToken: String)

data class AuthenticationSessionEntity(val sessionId: String)