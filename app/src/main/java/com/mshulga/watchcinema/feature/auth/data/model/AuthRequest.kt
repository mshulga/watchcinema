package com.mshulga.watchcinema.feature.auth.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * @author mshulga
 * @since  2019-08-05
 */
@Serializable
data class CreateSessionRequest(@SerialName("request_token") val requestToken: String)