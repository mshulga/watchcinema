package com.mshulga.watchcinema.feature.auth.data.model

import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationSessionEntity
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationTokenEntity

/**
 * @author mshulga
 * @since  2019-08-09
 */
class AuthToEntityMapper {
    fun mapTokenEntity(response: CreateTokenResponse) =
        AuthenticationTokenEntity(response.requestToken.orEmpty())

    fun mapSessionEntity(response: CreateSessionResponse) =
        AuthenticationSessionEntity(response.sessionId.orEmpty())
}