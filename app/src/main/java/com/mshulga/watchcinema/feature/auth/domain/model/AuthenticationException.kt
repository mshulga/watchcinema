package com.mshulga.watchcinema.feature.auth.domain.model

/**
 * @author mshulga
 * @since  2019-08-09
 */
class AuthenticationException(message: String) : RuntimeException(message)