package com.mshulga.watchcinema.feature.listContainer.presentation.presentation.presenter

import com.mshulga.core_navigation.FlowRouter
import com.mshulga.core_ui.flow.FlowPresenter
import com.mshulga.watchcinema.di.Screens
import moxy.MvpView
import javax.inject.Inject

/**
 * @author mshulga
 * @since  30.12.2019
 */
class ListContainerFlowPresenter @Inject constructor(
    flowRouter: FlowRouter
) : FlowPresenter<MvpView>(flowRouter) {
    override fun getLaunchScreen() = Screens.TabContainerList
}