package com.mshulga.watchcinema.feature.listContainer.presentation.presentation.view

import com.mshulga.core_ui.base.BaseFragment
import com.mshulga.watchcinema.R

/**
 * @author mshulga
 * @since  30.12.2019
 */
class TabContainerListFragment : BaseFragment() {

    override val layoutRes: Int = R.layout.fmt_tab_container_list
    override fun onBackPressed() {
    }

    companion object {
        fun getInstance() = TabContainerListFragment()
    }
}