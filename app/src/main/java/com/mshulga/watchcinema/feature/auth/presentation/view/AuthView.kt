package com.mshulga.watchcinema.feature.auth.presentation.view

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

/**
 * @author mshulga
 * @since  2019-08-08
 */
interface AuthView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLabel()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hideLabel()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setLabelText(text: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showWebView()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hideWebView()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun loadUrl(url: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(errorMessage: String)
}