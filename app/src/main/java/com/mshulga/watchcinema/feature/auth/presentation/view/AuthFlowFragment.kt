package com.mshulga.watchcinema.feature.auth.presentation.view

import com.mshulga.core_ui.flow.FlowFragment
import com.mshulga.watchcinema.feature.auth.presentation.presenter.AuthFlowPresenter
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

/**
 * @author mshulga
 * @since  2019-08-27
 */
class AuthFlowFragment : FlowFragment() {

    @InjectPresenter
    lateinit var presenter: AuthFlowPresenter

    @ProvidePresenter
    fun providePresenter(): AuthFlowPresenter = scope.getInstance(AuthFlowPresenter::class.java)

    companion object {
        fun getInstance() = AuthFlowFragment()
    }
}