package com.mshulga.watchcinema.feature.auth.data.repository

import com.mshulga.core_prefs.SharedPrefs
import com.mshulga.watchcinema.feature.auth.data.ds.AuthApi
import com.mshulga.watchcinema.feature.auth.data.model.AuthToEntityMapper
import com.mshulga.watchcinema.feature.auth.data.model.CreateSessionRequest
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationSessionEntity
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationTokenEntity
import javax.inject.Inject

/**
 * @author mshulga
 * @since  2019-08-09
 */
class AuthRepository @Inject constructor(
    private val authApi: AuthApi,
    private val toEntityMapper: AuthToEntityMapper,
    private val prefs: SharedPrefs
) {

    suspend fun createNewSession(requestToken: String): AuthenticationSessionEntity {
        val session = authApi.createNewSession(CreateSessionRequest(requestToken))
        val entity = toEntityMapper.mapSessionEntity(session)
        prefs.sessionId = entity.sessionId
        return entity
    }


    suspend fun getNewAuthenticationToken(): AuthenticationTokenEntity {
        val token = authApi.getNewAuthenticationToken()
        return toEntityMapper.mapTokenEntity(token)
    }
}