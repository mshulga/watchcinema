package com.mshulga.watchcinema.feature.auth.data.ds

import com.mshulga.watchcinema.BuildConfig
import com.mshulga.watchcinema.feature.auth.data.model.CreateSessionRequest
import com.mshulga.watchcinema.feature.auth.data.model.CreateSessionResponse
import com.mshulga.watchcinema.feature.auth.data.model.CreateTokenResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * @author mshulga
 * @since  2019-08-05
 */
interface AuthApi {

    @GET("authentication/token/new")
    suspend fun getNewAuthenticationToken(@Query("api_key") apiKey: String = BuildConfig.API_KEY) : CreateTokenResponse

    @POST("authentication/session/new")
    suspend fun createNewSession(@Body requestBody: CreateSessionRequest, @Query("api_key") apiKey: String = BuildConfig.API_KEY) : CreateSessionResponse
}