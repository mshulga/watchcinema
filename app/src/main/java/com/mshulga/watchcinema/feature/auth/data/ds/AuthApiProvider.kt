package com.mshulga.watchcinema.feature.auth.data.ds

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Provider

/**
 * @author mshulga
 * @since  2019-08-05
 */
class AuthApiProvider @Inject constructor(private val retrofit: Retrofit) : Provider<AuthApi> {

    override fun get(): AuthApi = retrofit.create(AuthApi::class.java)
}