package com.mshulga.watchcinema.feature.auth.presentation.view

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.annotation.RequiresApi
import com.mshulga.core_ui.base.BaseFragment
import com.mshulga.core_utils.extentions.gone
import com.mshulga.core_utils.extentions.show
import com.mshulga.watchcinema.R
import com.mshulga.watchcinema.feature.auth.data.model.AuthToEntityMapper
import com.mshulga.watchcinema.feature.auth.presentation.presenter.AuthPresenter
import kotlinx.android.synthetic.main.ac_auth.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module

/**
 * @author mshulga
 * @since  2019-08-04
 */
class AuthFragment : BaseFragment(), AuthView {

    override val layoutRes: Int = R.layout.ac_auth

    @InjectPresenter
    lateinit var presenter: AuthPresenter

    override fun installModules(scope: Scope) {
        scope.installModules(object : Module() {
            init {
                bind(AuthToEntityMapper::class.java).toInstance(AuthToEntityMapper())
            }
        })
    }

    @ProvidePresenter
    fun providePresenter() = scope.getInstance(AuthPresenter::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initWebView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return overrideUrlLoading(url)
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                return overrideUrlLoading(request.url.toString())
            }

            private fun overrideUrlLoading(url: String): Boolean {
                return presenter.onOverrideUrlLoading(url)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        webView.onResume()
    }

    override fun onPause() {
        super.onPause()
        webView.onPause()
    }

    override fun onDestroyView() {
        webView.destroy()
        super.onDestroyView()
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(context, errorMessage, LENGTH_LONG).show()
    }

    override fun showLabel() {
        label.show()
    }

    override fun setLabelText(text: String) {
        label.text = text
    }

    override fun hideLabel() {
        label.gone()
    }

    override fun showWebView() {
        webView.show()
    }

    override fun hideWebView() {
        webView.gone()
    }

    override fun loadUrl(url: String) {
        webView.loadUrl(url)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    companion object {
        fun getInstance(): AuthFragment = AuthFragment()
    }

}