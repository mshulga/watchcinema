package com.mshulga.watchcinema.feature.listContainer.presentation.presentation.view

import com.mshulga.core_ui.flow.FlowFragment
import com.mshulga.watchcinema.feature.listContainer.presentation.presentation.presenter.ListContainerFlowPresenter
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

/**
 * @author mshulga
 * @since  30.12.2019
 */
class ListContainerFlowFragment : FlowFragment() {

    @InjectPresenter
    lateinit var presenter: ListContainerFlowPresenter

    @ProvidePresenter
    fun providePresenter(): ListContainerFlowPresenter = scope.getInstance(ListContainerFlowPresenter::class.java)

    companion object {
        fun getInstance() = ListContainerFlowFragment()
    }
}