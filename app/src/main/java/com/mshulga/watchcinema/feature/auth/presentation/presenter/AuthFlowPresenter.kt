package com.mshulga.watchcinema.feature.auth.presentation.presenter

import com.mshulga.core_navigation.FlowRouter
import com.mshulga.core_ui.flow.FlowPresenter
import com.mshulga.watchcinema.di.Screens
import moxy.MvpView
import javax.inject.Inject

/**
 * @author mshulga
 * @since  2019-08-27
 */
class AuthFlowPresenter @Inject constructor(router: FlowRouter) : FlowPresenter<MvpView>(router) {
    override fun getLaunchScreen() = Screens.Auth
}