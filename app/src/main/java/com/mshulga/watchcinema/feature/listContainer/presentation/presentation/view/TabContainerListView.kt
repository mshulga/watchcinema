package com.mshulga.watchcinema.feature.listContainer.presentation.presentation.view

import moxy.MvpView

/**
 * @author mshulga
 * @since  30.12.2019
 */
interface TabContainerListView : MvpView {
}