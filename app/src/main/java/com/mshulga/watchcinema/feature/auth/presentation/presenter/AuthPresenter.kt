package com.mshulga.watchcinema.feature.auth.presentation.presenter

import com.mshulga.core_ui.base.BasePresenter
import com.mshulga.watchcinema.di.Screens
import com.mshulga.watchcinema.feature.auth.domain.interactor.AuthInteractor
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationSessionEntity
import com.mshulga.watchcinema.feature.auth.domain.model.AuthenticationTokenEntity
import com.mshulga.watchcinema.feature.auth.presentation.view.AuthView
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import moxy.InjectViewState
import javax.inject.Inject

/**
 * @author mshulga
 * @since  2019-08-08
 */
@InjectViewState
class AuthPresenter @Inject constructor(
    private val authInteractor: AuthInteractor,
    router: com.mshulga.core_navigation.FlowRouter
) : BasePresenter<AuthView>(router) {

    private lateinit var authenticateSuccessLink: String
    private lateinit var requestToken: String

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        getRequestToken()
    }

    fun onOverrideUrlLoading(url: String) =
        if (url == authenticateSuccessLink) {
            onAuthSuccess()
            true
        } else {
            viewState.loadUrl(url)
            false
        }

    private fun getRequestToken() {
        launch(CoroutineExceptionHandler { _, throwable -> viewState.showError(throwable.message.orEmpty()) }) {
            val token = authInteractor.getNewAuthenticationToken()
            showRequestToken(token)
        }

    }

    private fun showRequestToken(entity: AuthenticationTokenEntity) {
        with(viewState) {
            requestToken = entity.requestToken
            authenticateSuccessLink =
                String.format(AUTHENTICATE_SUCCESS_REDIRECT_LINK_FORMAT, requestToken)
            showLabel()
            showWebView()
            loadUrl(String.format(AUTHENTICATE_URL_FORMAT, requestToken))
        }
    }

    private fun onAuthSuccess() {
        launch(CoroutineExceptionHandler { _, throwable -> viewState.showError(throwable.message.orEmpty()) }) {
            val sessionEntity = authInteractor.createNewSession(requestToken)
            onSuccessFetchSessionId(sessionEntity)
        }
    }

    private fun onSuccessFetchSessionId(entity: AuthenticationSessionEntity) {
        router.newRootFlow(Screens.ListContainerFlow)
    }


    companion object {
        private const val AUTHENTICATE_SUCCESS_REDIRECT_LINK_FORMAT =
            "mshulga://watchcinema.com?request_token=%s&approved=true"
        private const val AUTHENTICATE_URL_FORMAT =
            "https://www.themoviedb.org/authenticate/%s?redirect_to=mshulga://watchcinema.com"
    }
}