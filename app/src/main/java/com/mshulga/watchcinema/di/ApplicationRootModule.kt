package com.mshulga.watchcinema.di

import android.content.Context
import toothpick.config.Module

/**
 * @author mshulga
 * @since  2019-08-23
 */
class ApplicationRootModule(context: Context) : Module() {
    init {
        bind(Context::class.java).toInstance(context)
    }
}