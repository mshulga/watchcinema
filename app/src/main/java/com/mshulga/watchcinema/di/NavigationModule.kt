package com.mshulga.watchcinema.di

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

/**
 * @author mshulga
 * @since  2019-10-15
 */
class NavigationModule : Module() {
    init {
        val cicerone = Cicerone.create()
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)
        bind(Router::class.java).toInstance(cicerone.router)
    }
}