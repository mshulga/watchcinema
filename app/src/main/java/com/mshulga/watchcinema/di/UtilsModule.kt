package com.mshulga.watchcinema.di

import com.mshulga.core_utils.ResourceProvider
import toothpick.config.Module

/**
 * @author mshulga
 * @since  2019-10-15
 */
class UtilsModule : Module() {
    init {
        bind(ResourceProvider::class.java).singleton()
    }
}