package com.mshulga.watchcinema.di

import com.mshulga.core_prefs.SharedPrefs
import toothpick.config.Module

/**
 * @author mshulga
 * @since  2019-10-15
 */
class StorageModule : Module() {
    init {
        bind(SharedPrefs::class.java).singleton()
    }
}