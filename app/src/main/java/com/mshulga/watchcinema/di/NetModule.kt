package com.mshulga.watchcinema.di

import com.mshulga.core_network.data.BaseUrl
import com.mshulga.core_network.provider.OkHTTPClientProvider
import com.mshulga.core_network.provider.RetrofitProvider
import com.mshulga.watchcinema.BuildConfig
import com.mshulga.watchcinema.feature.auth.data.ds.AuthApi
import com.mshulga.watchcinema.feature.auth.data.ds.AuthApiProvider
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import toothpick.config.Module

/**
 * @author mshulga
 * @since  2019-08-23
 */
class NetModule : Module() {
    init {
        bind(OkHttpClient::class.java).toProvider(OkHTTPClientProvider::class.java).singleton()
        bind(Retrofit::class.java).toProvider(RetrofitProvider::class.java).singleton()
        bind(String::class.java).withName(BaseUrl::class.java).toInstance(BuildConfig.BASE_URL)
        bind(AuthApi::class.java).toProvider(AuthApiProvider::class.java).singleton()
    }
}