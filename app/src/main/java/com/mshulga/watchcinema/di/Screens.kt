package com.mshulga.watchcinema.di

import com.mshulga.watchcinema.feature.auth.presentation.view.AuthFlowFragment
import com.mshulga.watchcinema.feature.auth.presentation.view.AuthFragment
import com.mshulga.watchcinema.feature.cinemaLists.presentation.view.CinemaListFlowFragment
import com.mshulga.watchcinema.feature.listContainer.presentation.presentation.view.ListContainerFlowFragment
import com.mshulga.watchcinema.feature.listContainer.presentation.presentation.view.TabContainerListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * @author mshulga
 * @since  2019-08-27
 */
object Screens {

    //Auth
    object AuthFlow : SupportAppScreen() {
        override fun getFragment() = AuthFlowFragment.getInstance()
    }

    object Auth : SupportAppScreen() {
        override fun getFragment() = AuthFragment.getInstance()
    }

    //Container
    object ListContainerFlow : SupportAppScreen() {
        override fun getFragment() = ListContainerFlowFragment.getInstance()
    }

    object TabContainerList : SupportAppScreen() {
        override fun getFragment() = TabContainerListFragment.getInstance()
    }

    //CinemaList
    object CinemaListFlow : SupportAppScreen() {
        override fun getFragment() = CinemaListFlowFragment.getInstance()
    }

}