package com.mshulga.watchcinema

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@Serializable
data class GeoDictionary(
    @SerialName("id")
    val id: Int = 1,
    @SerialName("level")
    val level: Int = 1,
    @SerialName("parent_id")
    val parentId: Int = 1,
    @SerialName("value")
    val value: String = "value123",
    @SerialName("zip_code")
    val zipCode: Int = 12341212
)

@Serializable
data class GeoDictionaryResponse(
    @SerialName("data")
    val data: List<GeoDictionary>
)

class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val json = Json(JsonConfiguration.Stable)
        val geoDictionary = GeoDictionary()
        val jsonValue = json.stringify(GeoDictionary.serializer(), geoDictionary)
        assertEquals(4, 2 + 2)
    }

    @Test
    fun addition_isCorrect1() {
        val json = Json(JsonConfiguration.Stable.copy(
            ignoreUnknownKeys = true
        ))
        val str = "{\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"level\": 0,\n" +
                "      \"parent_id\": 0,\n" +
                "      \"value\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"limit\": 0,\n" +
                "  \"messages\": [\n" +
                "    {\n" +
                "      \"code\": \"string\",\n" +
                "      \"context\": \"GLOBAL\",\n" +
                "      \"path\": \"string\",\n" +
                "      \"type\": \"ERROR\",\n" +
                "      \"value\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"offset\": 0,\n" +
                "  \"status\": \"OK\",\n" +
                "  \"technical_details\": {\n" +
                "    \"error_code\": \"string\",\n" +
                "    \"error_description\": \"string\",\n" +
                "    \"error_id\": \"string\"\n" +
                "  },\n" +
                "  \"total\": 0\n" +
                "}"
        val geoDictionaryResponse = json.parse(GeoDictionaryResponse.serializer(), str)
        assertEquals(4, 2 + 2)
    }
}
